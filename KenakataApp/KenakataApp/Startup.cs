﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KenakataApp.Startup))]
namespace KenakataApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
