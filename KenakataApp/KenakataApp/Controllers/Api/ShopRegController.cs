﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using KenakataApp.Models;

namespace KenakataApp.Controllers.Api
{
    public class ShopRegController : ApiController
    {
        private DiscountDbContext db = new DiscountDbContext();

        // GET api/ShopReg
        public IQueryable<ShopRegistration> GetShopRegistration()
        {
            return db.ShopRegistration;
        }

        // GET api/ShopReg/5
        [ResponseType(typeof(ShopRegistration))]
        public IHttpActionResult GetShopRegistration(int id)
        {
            ShopRegistration shopregistration = db.ShopRegistration.Find(id);
            if (shopregistration == null)
            {
                return NotFound();
            }

            return Ok(shopregistration);
        }

        // PUT api/ShopReg/5
        public IHttpActionResult PutShopRegistration(int id, ShopRegistration shopregistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shopregistration.Id)
            {
                return BadRequest();
            }

            db.Entry(shopregistration).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopRegistrationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/ShopReg
        [ResponseType(typeof(ShopRegistration))]
        public IHttpActionResult PostShopRegistration(ShopRegistration shopregistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ShopRegistration.Add(shopregistration);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shopregistration.Id }, shopregistration);
        }

        // DELETE api/ShopReg/5
        [ResponseType(typeof(ShopRegistration))]
        public IHttpActionResult DeleteShopRegistration(int id)
        {
            ShopRegistration shopregistration = db.ShopRegistration.Find(id);
            if (shopregistration == null)
            {
                return NotFound();
            }

            db.ShopRegistration.Remove(shopregistration);
            db.SaveChanges();

            return Ok(shopregistration);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShopRegistrationExists(int id)
        {
            return db.ShopRegistration.Count(e => e.Id == id) > 0;
        }
    }
}