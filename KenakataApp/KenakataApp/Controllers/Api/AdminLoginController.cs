﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using KenakataApp.Models;

namespace KenakataApp.Controllers.Api
{
    public class AdminLoginController : ApiController
    {
        private DiscountDbContext db = new DiscountDbContext();

        //// GET api/AdminLogin

        //[OutputCache(Duration = 100)]
        //public IQueryable<AdminLogin> GetAdminLogin()
        //{
            
        //    return db.AdminLogin;
        //}

        // GET api/AdminLogin/5
        [ResponseType(typeof(AdminLogin))]
        public IHttpActionResult GetAdminLogin(int id)
        {
            Controllers.AdminLoginController adminLoginController=new Controllers.AdminLoginController();
            AdminLogin adminlogin = db.AdminLogin.Find();

            //if (adminlogin == null)
            //{
            //    return NotFound();
            //}
                
            //else
            //{
            //} 
            var user = db.AdminLogin.Where(x => x.UserName == adminlogin.UserName && x.Password == adminlogin.Password).Count();
            if (user == 1)
            {
                adminLoginController.ViewBag.forget = "Not found";
                adminLoginController.Response.Redirect("http://www.kenakatabd.somee.com/discount/create#");
            }
            return NotFound();
        }

       

        // PUT api/AdminLogin/5
        public IHttpActionResult PutAdminLogin(int id, AdminLogin adminlogin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != adminlogin.Id)
            {
                return BadRequest();
            }

            db.Entry(adminlogin).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdminLoginExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/AdminLogin
        [ResponseType(typeof(AdminLogin))]
        public IHttpActionResult PostAdminLogin(AdminLogin adminlogin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AdminLogin.Add(adminlogin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = adminlogin.Id }, adminlogin);
        }

        // DELETE api/AdminLogin/5
        [ResponseType(typeof(AdminLogin))]
        public IHttpActionResult DeleteAdminLogin(int id)
        {
            AdminLogin adminlogin = db.AdminLogin.Find(id);
            if (adminlogin == null)
            {
                return NotFound();
            }

            db.AdminLogin.Remove(adminlogin);
            db.SaveChanges();

            return Ok(adminlogin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdminLoginExists(int id)
        {
            return db.AdminLogin.Count(e => e.Id == id) > 0;
        }
    }
}