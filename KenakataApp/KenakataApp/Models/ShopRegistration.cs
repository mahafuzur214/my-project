﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KenakataApp.Models
{
    public class ShopRegistration
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter shop name")]
        [Display(Name = "Shop Name")]
        public string ShopName { get; set; }

        [Required(ErrorMessage = "Please enter Shop Full Address")]
        [Display(Name = "Full Address")]
        public string ShopFullAddress { get; set; }

        [Required(ErrorMessage = "Please enter Owner Name")]
        [Display(Name = "Owners Name")]
        public string OwnerName { get; set; }

        [Required(ErrorMessage = "Please enter Phone")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^?([0-9]{3})?([0−9]3)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter Email")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [DataType(DataType.Password)]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Password must be 6-10 characters")] 

        public string   Password { get; set; }
        [Required(ErrorMessage = "Please confirm your password")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Password must be 6-10 characters")]
        [Display(Name = "Confirm Password")]
        public string   ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter Shop Type")]
        [Display(Name = "Shop Type")]
        public string ShopType { get; set; }

        [Required]
        [Display(Name = "Is Active")]
        public string   IsActive { get; set; }   

 
    }
}