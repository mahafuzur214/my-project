﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KenakataApp.Models
{
    public class Discount
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter shop name")]
        [Display(Name = "Shop Name")]
        public string ShopName { get; set; }

        [Required(ErrorMessage = "Please enter Shop Full Address")]
        [Display(Name = "Full Address")]
        public string ShopFullAddress { get; set; }

        [Required(ErrorMessage = "Please enterShop Map Adress")]
        [Display(Name = "Map Adress")]
        public string ShopMapAdress { get; set; }

        [Required(ErrorMessage = "Please enter Owner Name")]
        [Display(Name = "Owners Name")]
        public string OwnerName { get; set; }

        [Required(ErrorMessage = "Please enter Phone")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^?([0-9]{3})?([0−9]3)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter Email")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Shop Type")]
        [Display(Name = "Shop Type")]
        public string ShopType { get; set; }


        [Required(ErrorMessage = "Please enter Discount Amount")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        [Display(Name = "Discount")]
        public int  DiscountAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; } 


    }
}